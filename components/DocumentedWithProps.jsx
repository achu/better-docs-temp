import { h } from 'preact'
import PropTypes from 'prop-types'
/**
 * Some documented component
 *
 * @component
 * @example
 * return (
 *   <DocumentedWithProps text="Hello"/>
 * )
 */
const DocumentedWithProps = (props) => {
  const { text } = props
  return (
    <div>{text}</div>
  )
}

DocumentedWithProps.propTypes = {
  /**
   * Text is a text
   */
  text: PropTypes.string.isRequired,
}

export default DocumentedWithProps
