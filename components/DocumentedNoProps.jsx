import { h } from 'preact'

/**
 * Some documented react component without any props
 *
 * @component
 * @example
 * return (
 *   <DocumentedNoProps />
 * )
 */
const DocumentedNoProps = () => {
  return <div>Hello world</div>
}
export default DocumentedNoProps
