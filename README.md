# better-docs test
> [See generated docs](https://achu.gitlab.io/better-docs-temp)

This repo tests 2 (p)react components:
* **DocumentedNoProps** - A component with no props
* **DocumentedWithProps** - A component with props

## Steps
1. `npm i`
1. `npm start`